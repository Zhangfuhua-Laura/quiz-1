package Entity;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Random;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class Game {
    private int gameId;
    private String numbers;

    public Game() {
        this.gameId = new Random().nextInt();
        this.numbers = Integer.toString(new Random().nextInt(10000));
    }

    public Game(int gameId, String numbers) {
        this.gameId = gameId;
        this.numbers = numbers;
    }

    public int getGameId() {
        return gameId;
    }

    public String getNumbers() {
        return numbers;
    }
}
