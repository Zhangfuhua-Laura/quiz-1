package Entity;

import Entity.Game;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class GamesMap {
    private static Map<Integer, String> gamesMap = new HashMap<>();

    public static String add(Game game){
        return gamesMap.put(game.getGameId(), game.getNumbers());
    }

    public static String getString(Integer gameId){
        Iterator<Integer> gameItem = gamesMap.keySet().iterator();
        while (gameItem.hasNext()){
            if(gameItem.next() == gameId){
                return gamesMap.get(gameId);
            }
        }
        return "";
    }
}
