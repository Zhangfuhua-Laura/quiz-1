package com.twuc.webApp;

import Entity.Answer;
import Entity.Game;
import Entity.GamesMap;
import Service.GameCheck;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class GameController {
//    private AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp");

    @PostMapping("/games")
    @ResponseBody
    ResponseEntity<Integer> createGame(){
//        context.refresh();
//        Game game = context.getBean(Game.class);
        Game game = new Game();
        GamesMap.add(game);
        return ResponseEntity.status(201).header("Location", String.format("/api/games/%s", game.getGameId())).body(game.getGameId());
    }

    @GetMapping("/games/{gameId}")
    ResponseEntity<String> game(@PathVariable int gameId) throws JsonProcessingException {
        String response = new ObjectMapper().writeValueAsString(new Game(gameId, GamesMap.getString(gameId)));
        return ResponseEntity.status(200).body(response);
    }

    @PatchMapping("/games/{gameId}")
    ResponseEntity<String> checkCorrectAnswer(@RequestBody Answer answer, @PathVariable Integer gameId){
//        GameCheck.check(answer, gameId);
        String response = new ObjectMapper().writeValueAsString(Object);
        return ResponseEntity.status(200).body(response);
    }
}
