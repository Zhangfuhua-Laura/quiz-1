package Service;

import Entity.Answer;
import Entity.GamesMap;

public class GameCheck {

    public static boolean check(Answer answer, Integer gameId){
        if(GamesMap.getString(gameId) == answer.getAnswer())
            return true;
        return false;
    }

}
