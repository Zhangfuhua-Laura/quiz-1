package com.twuc.webApp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest
@AutoConfigureMockMvc
public class GameIntegrationTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    void should_return_201_when_post_api_game() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/games"))
                .andExpect(MockMvcResultMatchers.status().is(201))
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8));
    }

    @Test
    void should_get_correct_answer_when_given_correct_gameId() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/games/12"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("{\"gameId\":12,\"numbers\":\"\"}"));
    }

    @Test
    void should_return_true_when_patch_the_correct_answer() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.patch("/api/games/12")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"answer\":\"1234\"}"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.content().string("{\"hint\":\"4A0B\",\"correct\":\"true\"}"));
    }
}
